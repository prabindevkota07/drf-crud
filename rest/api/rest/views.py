from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Item, Category
from .serializer import ItemSerializer,CategorySerializer


# Create your views here.


class ItemViewSet(ModelViewSet):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
