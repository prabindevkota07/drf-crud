from rest_framework.routers import DefaultRouter
from .views import ItemViewSet, CategoryViewSet
from django.urls import path, include

router = DefaultRouter()
router.register('item', ItemViewSet)
router.register('category', CategoryViewSet)

urlpatterns = [

    path('api/', include(router.urls))
]
