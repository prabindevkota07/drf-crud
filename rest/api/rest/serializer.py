from rest_framework.serializers import ModelSerializer
from .models import Category, Item


class ItemSerializer(ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'
        depth = 1


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
